package ch02

object helloargs extends App {
  // Say hello to the first argument
  println("Hello, "+ args(0) +"!")
}