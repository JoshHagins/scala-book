package ch02

object forargs extends App {
  for (arg <- args) {
    println(arg)
  }
}