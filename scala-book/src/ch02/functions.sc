package chapter2

object functions {
	def max(x: Int, y: Int): Int = {
		if (x > y) x
		else y
	}                                         //> max: (x: Int, y: Int)Int
	
	def max2(x: Int, y: Int) = if (x > y) x else y
                                                  //> max2: (x: Int, y: Int)Int

	def greet() = println("Hello, world!")    //> greet: ()Unit
}