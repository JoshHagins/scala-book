package ch02

object echoargs extends App {
  var i = 0
  while (i < args.length) {
    if (i != 0) {
      print(" ")
    }
    print(args(i))
    i += 1
  }
  println()
}