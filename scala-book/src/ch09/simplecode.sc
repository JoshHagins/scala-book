package chapter9

object simplecode {
  def containsNeg(nums: List[Int]): Boolean = {
    var exists = false
    for (num <- nums) {
      if (num < 0) {
        exists = true
      }
    }
    exists
  }                                               //> containsNeg: (nums: List[Int])Boolean

  containsNeg(List(1, 2, 3, 4))                   //> res0: Boolean = false
  containsNeg(List(1, 2, -3, 4))                  //> res1: Boolean = true

  def containsNeg2(nums: List[Int]) = nums.exists(_ < 0)
                                                  //> containsNeg2: (nums: List[Int])Boolean

  containsNeg2(Nil)                               //> res2: Boolean = false
  containsNeg2(List(0, -1, -2))                   //> res3: Boolean = true

  def containsOdd(nums: List[Int]): Boolean = {
    var exists = false
    for (num <- nums) {
      if (num % 2 == 1) {
        exists = true
      }
    }
    exists
  }                                               //> containsOdd: (nums: List[Int])Boolean

  def containsOdd2(nums: List[Int]) = nums.exists(_ % 2 == 1)
                                                  //> containsOdd2: (nums: List[Int])Boolean

}