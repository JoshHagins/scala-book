package chapter9

import java.io.File
import java.io.PrintWriter

object controlstructs {
  def twice(op: Double => Double, x: Double) = op(op(x))
                                                  //> twice: (op: Double => Double, x: Double)Double
  twice(_ + 1, 5)                                 //> res0: Double = 7.0

  def withPrintWriter(file: File, op: PrintWriter => Unit) {
    val writer = new PrintWriter(file)
    try {
      op(writer)
    } finally {
      writer.close()
    }
  }                                               //> withPrintWriter: (file: java.io.File, op: java.io.PrintWriter => Unit)Unit

  withPrintWriter(
    new File("date.txt"),
    writer => writer.println(new java.util.Date))

  // Can use curly braces instead of parentheses if
  // passing exactly one argument
  println { "Hello, world!" }                     //> Hello, world!

  def withPrintWriter2(file: File)(op: PrintWriter => Unit) {
    val writer = new PrintWriter(file)
    try {
      op(writer)
    } finally {
      writer.close()
    }
  }                                               //> withPrintWriter2: (file: java.io.File)(op: java.io.PrintWriter => Unit)Unit

  val file = new File("date.txt")                 //> file  : java.io.File = date.txt

  withPrintWriter2(file) {
    writer => writer.println(new java.util.Date)
  }
}