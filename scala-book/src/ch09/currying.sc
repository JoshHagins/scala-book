package chapter9

object currying {
  def plainOldSum(x: Int, y: Int) = x + y         //> plainOldSum: (x: Int, y: Int)Int
  plainOldSum(1, 2)                               //> res0: Int = 3

  def curriedSum(x: Int)(y: Int) = x + y          //> curriedSum: (x: Int)(y: Int)Int
  curriedSum(1)(2)                                //> res1: Int = 3

  def first(x: Int) = (y: Int) => x + y           //> first: (x: Int)Int => Int
  val second = first(1)                           //> second  : Int => Int = <function1>
  second(2)                                       //> res2: Int = 3

  val onePlus = curriedSum(1)_                    //> onePlus  : Int => Int = <function1>
  onePlus(2)                                      //> res3: Int = 3

  val twoPlus = curriedSum(2)_                    //> twoPlus  : Int => Int = <function1>
  twoPlus(2)                                      //> res4: Int = 4
}