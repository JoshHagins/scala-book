package chapter9

object bynameparams {
  var assertionsEnabled = true                    //> assertionsEnabled  : Boolean = true
  
  def myAssert(predicate: () => Boolean) =
  	if (assertionsEnabled && !predicate())
  		throw new AssertionError          //> myAssert: (predicate: () => Boolean)Unit
  		
  myAssert(() => 5 > 3)
  
  def byNameAssert(predicate: => Boolean) =
  	if (assertionsEnabled && !predicate)
  		throw new AssertionError          //> byNameAssert: (predicate: => Boolean)Unit
  
  byNameAssert(5 > 3)
  
  def boolAssert(predicate: Boolean) =
  	if (assertionsEnabled && !predicate)
  		throw new AssertionError          //> boolAssert: (predicate: Boolean)Unit
  		
  boolAssert(5 > 3)
  
  assertionsEnabled = false
  
  // Will throw an exception
  // boolAssert(1 / 0 == 0)
  
  byNameAssert(1 / 0 == 0)
}