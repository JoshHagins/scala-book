package bobsrockets {
  package navigation {    
    private[bobsrockets] class Navigator {
      // No need to say bobsrockets.navigation.StarMap
      val map = new StarMap    
      
      protected[navigation] def useStarChart() {}
      
      class LegOfJourney {
        private[Navigator] val distance = 100
      }
      
      private[this] var speed = 200
    }
    
    class StarMap
    
    package launch {
      class Booster1
    }
    
    class MissionControl {
      val booster1 = new launch.Booster1
      val booster2 = new bobsrockets.launch.Booster2
      val booster3 = new _root_.launch.Booster3
    }
  }
  
  class Ship {
    // No need to say bobsrockets.navigation.Navigator
    val nav = new navigation.Navigator
  }
  
  package fleets {
    class Fleet {
      // No need to say bobsrockets.Ship
      def addShip() { new Ship }
    }
  }
  
  package launch {
    class Booster2
    
    import navigation._
    object Vehicle {
      private[launch] val guide = new Navigator
    }
  }
}