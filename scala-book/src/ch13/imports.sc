package ch13

object imports {
  // easy access to Fruit
  import bobsdelights.Fruit

  // easy access to all members of bobsdelights
  import bobsdelights._

  // easy access to all members of Fruits
  import bobsdelights.Fruits._

  def showFruit(fruit: Fruit) {
    import fruit._
    println(name + "s are " + color)
  }                                               //> showFruit: (fruit: bobsdelights.Fruit)Unit

  import java.util.regex

  class AStarB {
    // Accesses java.util.regex.Pattern
    val pat = regex.Pattern.compile("a*b")
  }
  
  // Imports just members Apple and Orange from object Fruits
  import Fruits.{Apple, Orange}
  
  // Imports Apple as McIntosh and Orange
  import Fruits.{Apple => McIntosh, Orange}
  
  import java.sql.{Date => SDate}
  
  import java.{sql => S}
  
  import Fruits.{_}
  
  // Imports all members of Fruits, renames Apple to McIntosh
  import Fruits.{Apple => McIntosh, _}
  
  // Imports all members of Fruits except Pear
  import Fruits.{Pear => _, _}
}