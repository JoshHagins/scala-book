package ch13

object accessmods {
  class Outer {
  	class Inner {
  		private def f() { println("f") }
  		class InnerMost {
  			f() // OK
  		}
  		new InnerMost
  	}
  	new Inner
  	// (new Inner).f() // error: f is not accessible
  }
  new Outer                                       //> f
                                                  //| res0: ch13.accessmods.Outer = ch13.accessmods$Outer@2bbf1be2
  
  class Super {
  	protected def f() { println("f") }
  }
  class Sub extends Super {
  	f()
  }
  class Other {
  	// (new Super).f() // error: f is not accessible
  }
  new Sub                                         //> f
                                                  //| res1: ch13.accessmods.Sub = ch13.accessmods$$anonfun$main$1$Sub$1@498b3b74
}