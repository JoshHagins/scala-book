package ch12

object phrog {
  val frog = new Frog                             //> frog  : ch12.Frog = green
  frog.philosophize()                             //> It ain't easy being green!

  val phil: Philosophical = frog                  //> phil  : ch12.Philosophical = green
  phil.philosophize()                             //> It ain't easy being green!

  val phrog: Philosophical = new Frog             //> phrog  : ch12.Philosophical = green
  phrog.philosophize()                            //> It ain't easy being green!
}