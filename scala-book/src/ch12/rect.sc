package ch12

object rect {
  val rect = new Rectangle(new Point(1, 1), new Point(10, 10))
                                                  //> rect  : ch12.Rectangle = ch12.Rectangle@5835fbdd
  rect.left                                       //> res0: Int = 1
  rect.right                                      //> res1: Int = 10
  rect.width                                      //> res2: Int = 9
}