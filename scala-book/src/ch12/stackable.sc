package ch12

object stackable {
  var queue = new BasicIntQueue                   //> queue  : ch12.BasicIntQueue = ch12.BasicIntQueue@70f00513
  queue.put(10)
  queue.put(20)
  queue.get()                                     //> res0: Int = 10
  queue.get()                                     //> res1: Int = 20
  
  class MyQueue extends BasicIntQueue with Doubling
  queue = new MyQueue
  queue.put(10)
  queue.get()                                     //> res2: Int = 20
  
  queue = new BasicIntQueue with Doubling
  queue.put(10)
  queue.get()                                     //> res3: Int = 20
  
  // Order of mixins matters
  // Methods in mixins called right to left
  
  queue = new BasicIntQueue with Incrementing with Filtering
  queue.put(-1); queue.put(0); queue.put(1)
  queue.get()                                     //> res4: Int = 1
  queue.get()                                     //> res5: Int = 2
  
  queue = new BasicIntQueue with Filtering with Incrementing
  queue.put(-1); queue.put(0); queue.put(1)
  queue.get()                                     //> res6: Int = 0
  queue.get()                                     //> res7: Int = 1
  queue.get()                                     //> res8: Int = 2
}