package ch12

object ordered {
  val half = new Rational(1, 2)                   //> half  : ch12.Rational = 1/2
  val third = new Rational(1, 3)                  //> third  : ch12.Rational = 1/3
  
  half < third                                    //> res0: Boolean = false
  half > third                                    //> res1: Boolean = true
  
  val big = new Rational(2, 4)                    //> big  : ch12.Rational = 1/2
  half == big                                     //> res2: Boolean = false
}