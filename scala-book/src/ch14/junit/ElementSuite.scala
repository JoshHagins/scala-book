package ch14
package junit

import org.scalatest.junit.JUnit3Suite
import Element.elem

class ElementSuite extends JUnit3Suite {
  
  def testUniformElement() {
    val ele = elem('x', 2, 3)
    assert(ele.width === 2)
    assert(ele.height === 3)
    
    intercept[IllegalArgumentException] {
      elem('x', -2, 3)
    }
  }
}