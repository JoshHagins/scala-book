package ch14
package junit

import _root_.junit.framework.TestCase
import _root_.junit.framework.Assert.{ assertEquals, fail }
import Element.elem

class ElementTestCase extends TestCase {

  def testUniformElement() {
    val ele = elem('x', 2, 3)
    assertEquals(2, ele.width)
    assertEquals(3, ele.height)

    try {
      elem('x', -2, 3)
      fail()
    } catch {
      case e: IllegalArgumentException => // expected
    }
  }
}