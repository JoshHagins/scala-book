package ch14
package scalatest

import org.scalatest.FunSuite
import Element.elem

class ElementFunSuite extends FunSuite {

  test("elem result should have passed width") {
    val ele = elem('x', 2, 3)
    assert(ele.width === 2)
  }

  test("elem should produce IllegalArgumentException when invoked with negative width") {
    intercept[IllegalArgumentException] {
      elem('x', -2, 3)
    }
  }
}