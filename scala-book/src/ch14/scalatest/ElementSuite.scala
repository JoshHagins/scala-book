package ch14
package scalatest

import org.scalatest.Suite
import Element.elem

class ElementSuite extends Suite {

  def testUniformElement() {
    val ele = elem('x', 2, 3)
    assert(ele.width === 2)
  }
  
  def testNegativeWidthThrowsIAE() {
    intercept[IllegalArgumentException] {
      elem('x', -2, 3)
    }
  }
}