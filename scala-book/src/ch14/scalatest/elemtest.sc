package ch14
package scalatest

object elemtest {
  (new ElementFunSuite).execute()                 //> [32mElementFunSuite:[0m
                                                  //| [32m- elem result should have passed width[0m
                                                  //| [32m- elem should produce IllegalArgumentException when invoked with negativ
                                                  //| e width[0m

  (new ElementSpec).execute()                     //> [32mElementSpec:[0m
                                                  //| [32mA UniformElement[0m
                                                  //| [32m- should have a width equal to the passed value[0m
                                                  //| [32m- should have a height equal to the passed value[0m
                                                  //| [32m- should throw an IAE if passed a negative width[0m
}