package ch14
package testng

import org.scalatest.testng.TestNGSuite
import org.testng.annotations.Test
import Element.elem

class ElementSuite extends TestNGSuite {

  @Test def verifyUniformElement() {
    val ele = elem('x', 2, 3)
    assert(ele.width === 2)
    assert(ele.height === 3)

    intercept[IllegalArgumentException] {
      elem('x', -2, 3)
    }
  }
}