package chapter3

object arrays {
  val big = new java.math.BigInteger("12345")     //> big  : java.math.BigInteger = 12345

  val greetStrings = new Array[String](3)         //> greetStrings  : Array[String] = Array(null, null, null)
  greetStrings(0) = "Hello"
  greetStrings(1) = ", "
  greetStrings(2) = "world!\n"

  for (i <- 0 to 2) {
    print(greetStrings(i))                        //> Hello, world!
  }

  val greetStrings2 = new Array[String](3)        //> greetStrings2  : Array[String] = Array(null, null, null)
  greetStrings2.update(0, "Hello")
  greetStrings2.update(1, ", ")
  greetStrings2.update(2, "world!\n")

  for (i <- 0.to(2)) {
    print(greetStrings2.apply(i))                 //> Hello, world!
  }

  val numNames = Array("zero", "one", "two")      //> numNames  : Array[String] = Array(zero, one, two)
}