package chapter3

object maps {
  val romanNumeral = Map(
    1 -> "I", 2 -> "II", 3 -> "III", 4 -> "IV", 5 -> "V")
                                                  //> romanNumeral  : scala.collection.immutable.Map[Int,String] = Map(5 -> V, 1 -
                                                  //| > I, 2 -> II, 3 -> III, 4 -> IV)
  println(romanNumeral(4))                        //> IV

  import scala.collection.mutable.Map

  val treasureMap = Map[Int, String]()            //> treasureMap  : scala.collection.mutable.Map[Int,String] = Map()
  treasureMap += (1 -> "Go to island.")           //> res0: chapter3.maps.treasureMap.type = Map(1 -> Go to island.)
  treasureMap += (2 -> "Find bix X on ground.")   //> res1: chapter3.maps.treasureMap.type = Map(2 -> Find bix X on ground., 1 -> 
                                                  //| Go to island.)
  treasureMap += (3 -> "Dig.")                    //> res2: chapter3.maps.treasureMap.type = Map(2 -> Find bix X on ground., 1 -> 
                                                  //| Go to island., 3 -> Dig.)
  println(treasureMap(2))                         //> Find bix X on ground.
}