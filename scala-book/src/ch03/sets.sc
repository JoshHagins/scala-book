package chapter3

object sets {
  var jetSet = Set("Boeing", "Airbus")            //> jetSet  : scala.collection.immutable.Set[String] = Set(Boeing, Airbus)
  jetSet += "Lear"
  println(jetSet.contains("Cessna"))              //> false
  
  import scala.collection.mutable.Set
  
  val movieSet = Set("Hitch", "Poltergeist")      //> movieSet  : scala.collection.mutable.Set[String] = Set(Poltergeist, Hitch)
  movieSet += "Shrek"                             //> res0: chapter3.sets.movieSet.type = Set(Poltergeist, Shrek, Hitch)
  println(movieSet)                               //> Set(Poltergeist, Shrek, Hitch)
  
  import scala.collection.immutable.HashSet
  
  val hashSet = HashSet("Tomatoes", "Chilies")    //> hashSet  : scala.collection.immutable.HashSet[String] = Set(Chilies, Tomatoe
                                                  //| s)
  println(hashSet + "Coriander")                  //> Set(Chilies, Tomatoes, Coriander)
}