package chapter3

object lists {
  val oneTwoThree = List(1, 2, 3)                 //> oneTwoThree  : List[Int] = List(1, 2, 3)
  
  val oneTwo = List(1, 2)                         //> oneTwo  : List[Int] = List(1, 2)
  val threeFour = List(3, 4)                      //> threeFour  : List[Int] = List(3, 4)
  val oneTwoThreeFour = oneTwo ::: threeFour      //> oneTwoThreeFour  : List[Int] = List(1, 2, 3, 4)
  println(oneTwo +" and "+ threeFour +" were not mutated.")
                                                  //> List(1, 2) and List(3, 4) were not mutated.
  println("Thus, "+ oneTwoThreeFour +" is a new list.")
                                                  //> Thus, List(1, 2, 3, 4) is a new list.
  
  val twoThree = List(2, 3)                       //> twoThree  : List[Int] = List(2, 3)
  val oneTwoThree2 = 1 :: twoThree                //> oneTwoThree2  : List[Int] = List(1, 2, 3)
  println(oneTwoThree2)                           //> List(1, 2, 3)
  
  val oneTwoThree3 = 1 :: 2 :: 3 :: Nil           //> oneTwoThree3  : List[Int] = List(1, 2, 3)
  println(oneTwoThree3)                           //> List(1, 2, 3)
}