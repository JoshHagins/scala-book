package chapter3

object tuples {
  val pair = (99, "Luftballons")                  //> pair  : (Int, String) = (99,Luftballons)
  println(pair._1)                                //> 99
  println(pair._2)                                //> Luftballons
  ('u', 'r', "the", 1, 4, "me")                   //> res0: (Char, Char, String, Int, Int, String) = (u,r,the,1,4,me)
}