package chapter3

object listmethods {
  List()                                          //> res0: List[Nothing] = List()

  Nil                                             //> res1: scala.collection.immutable.Nil.type = List()

  List("Cool", "tools", "rule")                   //> res2: List[String] = List(Cool, tools, rule)

  val thrill = "Will" :: "fill" :: "until" :: Nil //> thrill  : List[String] = List(Will, fill, until)

  List("a", "b") ::: List("c", "d")               //> res3: List[String] = List(a, b, c, d)

  thrill(2)                                       //> res4: String = until

  thrill.count(s => s.length == 4)                //> res5: Int = 2

  thrill.drop(2)                                  //> res6: List[String] = List(until)

  thrill.dropRight(2)                             //> res7: List[String] = List(Will)

  thrill.exists(s => s == "until")                //> res8: Boolean = true

  thrill.filter(s => s.length == 4)               //> res9: List[String] = List(Will, fill)

  thrill.forall(s => s.endsWith("l"))             //> res10: Boolean = true

  thrill.foreach(s => print(s))                   //> Willfilluntil

  thrill.foreach(print)                           //> Willfilluntil

  thrill.head                                     //> res11: String = Will

  thrill.init                                     //> res12: List[String] = List(Will, fill)

  thrill.isEmpty                                  //> res13: Boolean = false

  thrill.last                                     //> res14: String = until

  thrill.length                                   //> res15: Int = 3

  thrill.map(s => s + "y")                        //> res16: List[String] = List(Willy, filly, untily)

  thrill.mkString(", ")                           //> res17: String = Will, fill, until

  thrill.filterNot(s => s.length == 4)            //> res18: List[String] = List(until)

  thrill.reverse                                  //> res19: List[String] = List(until, fill, Will)

  thrill.sortWith((s, t) => s.charAt(0).toLower < t.charAt(0).toLower)
                                                  //> res20: List[String] = List(fill, until, Will)

  thrill.tail                                     //> res21: List[String] = List(fill, until)
}