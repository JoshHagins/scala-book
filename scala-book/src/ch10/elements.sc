package ch10

import Element.elem

object elements {
  val e: Element = elem(Array("hello"))           //> e  : ch10.Element = hello
  val e1: Element = elem(Array("hello", "world")) //> e1  : ch10.Element = hello
                                                  //| world
  val ae: Element = elem("hello")                 //> ae  : ch10.Element = hello
  val e2: Element = ae                            //> e2  : ch10.Element = hello
  val e3: Element = elem('x', 2, 3)               //> e3  : ch10.Element = xx
                                                  //| xx
                                                  //| xx
  
}