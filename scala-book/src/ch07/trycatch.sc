package chapter7

object trycatch {
  def half(n: Int) =
    if (n % 2 == 0)
      n / 2
    else
      throw new IllegalArgumentException("n must be even")
                                                  //> half: (n: Int)Int

  import java.io.FileReader
  import java.io.FileNotFoundException
  import java.io.IOException

  try {
    val file = new FileReader("input.txt")
    try {
      // Use the file
    } finally {
      file.close() // Be sure to close the file
    }
  } catch {
    case ex: FileNotFoundException => // Handle missing file
    case ex: IOException           => // Handle other I/O error
  }

  import java.net.URL
  import java.net.MalformedURLException

  def urlFor(path: String) =
    try {
      new URL(path)
    } catch {
      case e: MalformedURLException =>
        new URL("http://www.scala-lang.org")
    }                                             //> urlFor: (path: String)java.net.URL
    
   def f(): Int = try { return 1 } finally { return 2 }
                                                  //> f: ()Int
   f()                                            //> res0: Int = 2
   
   def g(): Int = try { 1 } finally { 2 }         //> g: ()Int
   g()                                            //> res1: Int = 1
}