package chapter7

object forexprs {
  val filesHere = (new java.io.File(".")).listFiles
                                                  //> filesHere  : Array[java.io.File] = Array(./eclipse, ./eclipse.ini)

  for (file <- filesHere) {
    println(file)                                 //> ./eclipse
                                                  //| ./eclipse.ini
  }

  for (i <- 1 to 4) {
    println("Iteration " + i)                     //> Iteration 1
                                                  //| Iteration 2
                                                  //| Iteration 3
                                                  //| Iteration 4
  }

  for (i <- 1 until 4) {
    println("Iteration " + i)                     //> Iteration 1
                                                  //| Iteration 2
                                                  //| Iteration 3
  }

  // Not common in Scala...
  for (i <- 0 to filesHere.length - 1) {
    println(filesHere(i))                         //> ./eclipse
                                                  //| ./eclipse.ini
  }

  for (file <- filesHere if file.getName.endsWith(".scala")) {
    println(file)
  }

  for {
    file <- filesHere
    if file.isFile
    if file.getName.endsWith(".scala")
  } println(file)

  def fileLines(file: java.io.File) =
    scala.io.Source.fromFile(file).getLines().toList
                                                  //> fileLines: (file: java.io.File)List[String]

  def grep(pattern: String) {
    for {
      file <- filesHere
      if file.getName.endsWith(".scala")
      line <- fileLines(file)
      trimmed = line.trim
      if trimmed.matches(pattern)
    } println(file + ": " + trimmed)
  }                                               //> grep: (pattern: String)Unit

  grep(".*gcd.*")

  def scalaFiles =
    for {
      file <- filesHere
      if file.getName.endsWith(".scala")
    } yield file                                  //> scalaFiles: => Array[java.io.File]

  def forLineLengths =
    for {
      file <- filesHere
      if file.getName.endsWith(".scala")
      line <- fileLines(file)
      trimmed = line.trim
      if trimmed.matches(".*for.*")
    } yield trimmed.length                        //> forLineLengths: => Array[Int]
}