package chapter7

object ifexprs {
	val args = List()                         //> args  : List[Nothing] = List()
	
  var filename = "default.txt"                    //> filename  : String = default.txt
  if (!args.isEmpty)
  	filename = args(0)
  	
  val filename2 =
  	if (!args.isEmpty) args(0)
  	else "default.txt"                        //> filename2  : String = default.txt
}