package chapter7

object matchexprs {
	val args = List("salt")                   //> args  : List[String] = List(salt)

  val firstArg = if (args.length > 0) args(0) else ""
                                                  //> firstArg  : String = salt

  val friend = firstArg match {
    case "salt"  => "pepper"
    case "chips" => "salsa"
    case "eggs"  => "bacon"
    case _       => "huh?"
  }                                               //> friend  : String = pepper
  
  println(friend)                                 //> pepper
  
  // Java code:
  // int i = 0;
  // boolean foundIt = false;
  // while (i < args.length) {
  //   if (args[i].startsWith("-")) {
  //     i = i + 1;
  //     continue;
  //   }
  //   if (args[i].endsWith(".scala")) {
  //     foundIt = true;
  //     break;
  //   }
  //   i = i + 1
  // }
  
  // Scala code:
  var i = 0                                       //> i  : Int = 0
  var foundIt = false                             //> foundIt  : Boolean = false
  
  while (i < args.length && !foundIt) {
  	if (!args(i).startsWith("-"))
  	  if (args(i).endsWith(".scala"))
  	    foundIt = true
  	i = i + 1
  }
  
  def searchFrom(i: Int): Int =
  	if (i >= args.length) -1
  	else if (args(i).startsWith("-")) searchFrom(i + 1)
  	else if (args(i).endsWith(".scala")) i
  	else searchFrom(i + 1)                    //> searchFrom: (i: Int)Int
  	
  searchFrom(0)                                   //> res0: Int = -1
  
  import scala.util.control.Breaks._
  import java.io._
  
  val in = new BufferedReader(new InputStreamReader(System.in))
                                                  //> in  : java.io.BufferedReader = java.io.BufferedReader@5ba78787
  
  breakable {
  	while (true) {
  		println("? ")
  		if (in.readLine() != "") break
  	}
  }                                               //> ? |
}