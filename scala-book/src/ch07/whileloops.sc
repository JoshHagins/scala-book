package chapter7

object whileloops {
  def gcdLoop(x: Long, y: Long): Long = {
    var a = x
    var b = y
    while (a != 0) {
      val temp = a
      a = b % a
      b = temp
    }
    b
  }                                               //> gcdLoop: (x: Long, y: Long)Long

  var line = ""                                   //> line  : String = ""
  do {
    //line = readLine()
    println("Read: " + line)
  } while (line != "")                            //> Read: 

  def greet() { println("hi") }                   //> greet: ()Unit
  greet()                                         //> hi
  
  def gcd(x: Long, y: Long): Long =
  	if (y == 0) x else gcd(y, x % y)          //> gcd: (x: Long, y: Long)Long
}