package chapter5

object precedenceassociativity {
  2 + 2 * 7                                       //> res0: Int = 16
  
  (2 + 2) * 7                                     //> res1: Int = 28
  
  2 << 2 + 2                                      //> res2: Int = 32
  
  2 + 2 << 2                                      //> res3: Int = 16
}