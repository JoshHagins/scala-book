package chapter5

object integers {
  val hex = 0x5                                   //> hex  : Int = 5
  
  val hex2 = 0x00FF                               //> hex2  : Int = 255
  
  val magic = 0xcafebabe                          //> magic  : Int = -889275714
  
  val oct = 035                                   //> oct  : Int = 29
  
  val nov = 0777                                  //> nov  : Int = 511
  
  val dec = 0321                                  //> dec  : Int = 209

	val dec1 = 31                             //> dec1  : Int = 31
	
	val dec2 = 255                            //> dec2  : Int = 255
	
	val dec3 = 20                             //> dec3  : Int = 20
	
	val prog = 0XCAFEBABEL                    //> prog  : Long = 3405691582
	
	val tower = 35L                           //> tower  : Long = 35
	
	val of = 31l                              //> of  : Long = 31
	
	val little: Short = 367                   //> little  : Short = 367
	
	val littler: Byte = 38                    //> littler  : Byte = 38
}