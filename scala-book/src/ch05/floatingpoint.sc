package chapter5

object floatingpoint {
	val big = 1.2345                          //> big  : Double = 1.2345
	
	val bigger = 1.2345e1                     //> bigger  : Double = 12.345
	
	val biggerStill = 123E45                  //> biggerStill  : Double = 1.23E47
	
	val little = 1.2345F                      //> little  : Float = 1.2345
	
	val littleBigger = 3e5f                   //> littleBigger  : Float = 300000.0
	
	val anotherDouble = 3e5                   //> anotherDouble  : Double = 300000.0
	
	val yetAnother = 3e5D                     //> yetAnother  : Double = 300000.0
}