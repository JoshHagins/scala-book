package chapter5

object characters {
	val a = 'A'                               //> a  : Char = A
	
	val c = '\101'                            //> c  : Char = A
	
	val d = '\u0041'                          //> d  : Char = A
	
	val f = '\u0044'                          //> f  : Char = D
	
	val backslash = '\\'                      //> backslash  : Char = \
}