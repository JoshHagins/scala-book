package chapter5

object strings {
  val hello = "hello"                             //> hello  : String = hello
  
  val escapes = "\\\"\'"                          //> escapes  : String = \"'
  
  println("""|Welcome to Ultamix 3000.
             |Type "HELP" for help.""".stripMargin)
                                                  //> Welcome to Ultamix 3000.
                                                  //| Type "HELP" for help.
}