package chapter5

object opmethods {
  val sum = 1 + 2                                 //> sum  : Int = 3
  
  val sumMore = (1).+(2)                          //> sumMore  : Int = 3
  
  val longSum = 1 + 2L                            //> longSum  : Long = 3
  
  val s = "Hello, world!"                         //> s  : String = Hello, world!
  s indexOf 'o'                                   //> res0: Int = 4
  s indexOf ('o', 5)                              //> res1: Int = 8
  
  val neg = -2.0                                  //> neg  : Double = -2.0
  
  (2.0).unary_-                                   //> res2: Double = -2.0
  
  s.toLowerCase                                   //> res3: String = hello, world!
  
  s toLowerCase                                   //> res4: String = hello, world!
}