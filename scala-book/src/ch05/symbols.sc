package chapter5

object symbols {
  def updateRecordByName(r: Symbol, value: Any) {
  	// code goes here
  }                                               //> updateRecordByName: (r: Symbol, value: Any)Unit
  
  updateRecordByName('favoriteAlbum, "OK Computer")
  
  val s = 'aSymbol                                //> s  : Symbol = 'aSymbol
  s.name                                          //> res0: String = aSymbol
}