package chapter5

object booleans {
  val bool = true                                 //> bool  : Boolean = true
  val fool = false                                //> fool  : Boolean = false
}