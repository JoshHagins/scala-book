package chapter5

object bitwise {
  1 & 2                                           //> res0: Int(0) = 0
  
  1 | 2                                           //> res1: Int(3) = 3
  
  1 ^ 3                                           //> res2: Int(2) = 2
  
  ~1                                              //> res3: Int = -2
  
  val sr = -1 >> 31                               //> sr  : Int = -1
  
  val usr = -1 >>> 31                             //> usr  : Int = 1
  
  val sl = 1 << 2                                 //> sl  : Int = 4
}