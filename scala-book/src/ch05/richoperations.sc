package chapter5

object richoperations {
  0 max 5                                         //> res0: Int = 5
  
  0 min 5                                         //> res1: Int = 0
  
  2.7.abs                                         //> res2: Double = 2.7
  
  val x = -2.7 abs                                //> x  : Double = 2.7
  
  val y = -2.7 round                              //> y  : Long = -3
  
  1.5 isInfinity                                  //> res3: Boolean = false
  
  (1.0 / 0) isInfinity                            //> res4: Boolean = true
  
  4 to 6                                          //> res5: scala.collection.immutable.Range.Inclusive = Range(4, 5, 6)
  
  "bob" capitalize                                //> res6: String = Bob
  
  "robert" drop 2                                 //> res7: String = bert
}