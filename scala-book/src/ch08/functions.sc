package chapter8

object functions {
  var increase = (x: Int) => x + 1                //> increase  : Int => Int = <function1>
  increase(10)                                    //> res0: Int = 11

  increase = (x: Int) => x + 9999
  increase(10)                                    //> res1: Int = 10009

  increase = (x: Int) => {
    println("We")
    println("are")
    println("here!")
    x + 1
  }
  increase(10)                                    //> We
                                                  //| are
                                                  //| here!
                                                  //| res2: Int = 11

  val someNumbers = List(-11, -10, -5, 0, 5, 10)  //> someNumbers  : List[Int] = List(-11, -10, -5, 0, 5, 10)
  someNumbers.foreach((x: Int) => println(x))     //> -11
                                                  //| -10
                                                  //| -5
                                                  //| 0
                                                  //| 5
                                                  //| 10
  
  someNumbers.filter((x: Int) => x > 0)           //> res3: List[Int] = List(5, 10)
  someNumbers.filter((x) => x > 0)                //> res4: List[Int] = List(5, 10)
  someNumbers.filter(x => x > 0)                  //> res5: List[Int] = List(5, 10)
  
  someNumbers.filter(_ > 0)                       //> res6: List[Int] = List(5, 10)

	val f = (_: Int) + (_: Int)               //> f  : (Int, Int) => Int = <function2>
	f(5, 10)                                  //> res7: Int = 15
	
	someNumbers.foreach(println _)            //> -11
                                                  //| -10
                                                  //| -5
                                                  //| 0
                                                  //| 5
                                                  //| 10
	
	def sum(a: Int, b: Int, c: Int) = a + b + c
                                                  //> sum: (a: Int, b: Int, c: Int)Int
  sum(1, 2, 3)                                    //> res8: Int = 6
	
	val a = sum _                             //> a  : (Int, Int, Int) => Int = <function3>
	a(1, 2, 3)                                //> res9: Int = 6
	
	a.apply(1, 2, 3)                          //> res10: Int = 6
	
	val b = sum(1, _: Int, 3)                 //> b  : Int => Int = <function1>
	b(2)                                      //> res11: Int = 6
	b(5)                                      //> res12: Int = 9
	
	someNumbers.foreach(println)              //> -11
                                                  //| -10
                                                  //| -5
                                                  //| 0
                                                  //| 5
                                                  //| 10
	
	val d = sum(_: Int, 2, _: Int)            //> d  : (Int, Int) => Int = <function2>
	d(1, 3)                                   //> res13: Int = 6
	
	
	
}