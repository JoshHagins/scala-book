package chapter8

object specialcallforms {
  def echo(args: String*) =
    for (arg <- args) println(arg)                //> echo: (args: String*)Unit

  echo()
  echo("one")                                     //> one
  echo("hello", "world")                          //> hello
                                                  //| world

  val arr = Array("What's", "up", "doc?")         //> arr  : Array[String] = Array(What's, up, doc?)
  echo(arr: _*)                                   //> What's
                                                  //| up
                                                  //| doc?
  def speed(distance: Float, time: Float): Float =
    distance / time                               //> speed: (distance: Float, time: Float)Float

  speed(100, 10)                                  //> res0: Float = 10.0
  speed(distance = 100, time = 10)                //> res1: Float = 10.0
  speed(time = 10, distance = 100)                //> res2: Float = 10.0

  def printTime(out: java.io.PrintStream = Console.out) =
    out.println("time = " + System.currentTimeMillis())
                                                  //> printTime: (out: java.io.PrintStream)Unit

  printTime()                                     //> time = 1402069964118
  printTime(Console.err)                          //> time = 1402069964119

  def printTime2(out: java.io.PrintStream = Console.out,
                 divisor: Int = 1) =
    out.println("time = " + System.currentTimeMillis() / divisor)
                                                  //> printTime2: (out: java.io.PrintStream, divisor: Int)Unit
    
  printTime2(out = Console.err)                   //> time = 1402069964119
  printTime2(divisor = 1000)                      //> time = 1402069964
}