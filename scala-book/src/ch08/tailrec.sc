package chapter8

object tailrec {
  def isGoodEnough(guess: Double) = guess < 0     //> isGoodEnough: (guess: Double)Boolean
  def improve(guess: Double) = guess - 1          //> improve: (guess: Double)Double

  def approximate(guess: Double): Double =
    if (isGoodEnough(guess)) guess
    else approximate(improve(guess))              //> approximate: (guess: Double)Double

  def approximateLoop(initialGuess: Double): Double = {
    var guess = initialGuess
    while (!isGoodEnough(guess)) {
      guess = improve(guess)
    }
    guess
  }                                               //> approximateLoop: (initialGuess: Double)Double

  def boom(x: Int): Int =
    if (x == 0) throw new Exception("boom!")
    else boom(x - 1) + 1                          //> boom: (x: Int)Int

  //boom(3)

  def bang(x: Int): Int =
    if (x == 0) throw new Exception("bang!")
    else bang(x - 1)                              //> bang: (x: Int)Int

  //bang(5)

  // No tail-call optimization for indirect recursion
  def isEven(x: Int): Boolean =
    if (x == 0) true else isOdd(x - 1)            //> isEven: (x: Int)Boolean
  def isOdd(x: Int): Boolean =
    if (x == 0) false else isEven(x - 1)          //> isOdd: (x: Int)Boolean

  // No tail-call optimization for function values
  def nestedFun(x: Int) {
    val funValue = nestedFun _
    if (x != 0) { println(x); funValue(x - 1) }
  }                                               //> nestedFun: (x: Int)Unit
}