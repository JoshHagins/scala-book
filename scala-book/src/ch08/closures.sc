package chapter8

object closures {
  // more not defined in scope
  // (x: Int) => x + more

  var more = 1                                    //> more  : Int = 1
  val addMore = (x: Int) => x + more              //> addMore  : Int => Int = <function1>
  addMore(10)                                     //> res0: Int = 11

  more = 9999
  addMore(10)                                     //> res1: Int = 10009

  val someNumbers = List(-11, -10, -5, 0, 5, 10)  //> someNumbers  : List[Int] = List(-11, -10, -5, 0, 5, 10)
  var sum = 0                                     //> sum  : Int = 0
  someNumbers.foreach(sum += _)
	sum                                       //> res2: Int = -11
	
	def makeIncreaser(more: Int) = (x: Int) => x + more
                                                  //> makeIncreaser: (more: Int)Int => Int
	
	val inc1 = makeIncreaser(1)               //> inc1  : Int => Int = <function1>
	val inc9999 = makeIncreaser(9999)         //> inc9999  : Int => Int = <function1>
	
	inc1(10)                                  //> res3: Int = 11
	inc9999(10)                               //> res4: Int = 10009
}