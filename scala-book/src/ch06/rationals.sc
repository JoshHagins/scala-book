package chapter6

object rationals {
  new Rational(1, 3)                              //> res0: chapter6.Rational = 1/3
  new Rational(5, 7)                              //> res1: chapter6.Rational = 5/7

  val r = new Rational(1, 2)                      //> r  : chapter6.Rational = 1/2
  r.numer                                         //> res2: Int = 1
  r.denom                                         //> res3: Int = 2

  new Rational(3)                                 //> res4: chapter6.Rational = 3/1

  new Rational(66, 42)                            //> res5: chapter6.Rational = 11/7

  val x = new Rational(1, 2)                      //> x  : chapter6.Rational = 1/2
  val y = new Rational(2, 3)                      //> y  : chapter6.Rational = 2/3
  x + y                                           //> res6: chapter6.Rational = 7/6
  x.+(y)                                          //> res7: chapter6.Rational = 7/6

  x + x * y                                       //> res8: chapter6.Rational = 5/6
  (x + x) * y                                     //> res9: chapter6.Rational = 2/3
  x + (x * y)                                     //> res10: chapter6.Rational = 5/6

  implicit def intToRational(x: Int) = new Rational(x)
                                                  //> intToRational: (x: Int)chapter6.Rational
  
  (x / 7) + (1 - y)                               //> res11: chapter6.Rational = 17/42
}