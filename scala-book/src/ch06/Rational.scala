package ch06

class Rational(n: Int, d: Int) {
  require(d != 0)

  private val g = gcd(n.abs, d.abs)
  val numer: Int = n / g
  val denom: Int = d / g

  def this(n: Int) = this(n, 1) // auxiliary constructor

  override def toString = numer + "/" + denom

  def + (that: Rational): Rational =
    new Rational(
      numer * that.denom + that.numer * denom,
      denom * that.denom)
  
  def + (i: Int): Rational =
    new Rational(numer + i * denom, denom)
  
  def - (that: Rational): Rational =
    this + new Rational(-that.numer, that.denom)
  
  def - (i: Int): Rational =
    new Rational(numer - i * denom, denom)

  def * (that: Rational): Rational =
    new Rational(numer * that.numer, denom * that.denom)
  
  def * (i: Int): Rational =
    new Rational(numer * i, denom)
  
  def / (that: Rational): Rational =
    this * new Rational(that.denom, that.numer)
  
  def / (i: Int): Rational =
    new Rational(numer, i * denom)

  private def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)
}