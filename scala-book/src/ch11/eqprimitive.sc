package ch11

object eqprimitive {
	def isEqual(x: Int, y: Int) = x == y      //> isEqual: (x: Int, y: Int)Boolean
	
	isEqual(421, 421)                         //> res0: Boolean = true
	
	def isEqualAny(x: Any, y: Any) = x == y   //> isEqualAny: (x: Any, y: Any)Boolean
	
	isEqualAny(421, 421)                      //> res1: Boolean = true
	
	val x = "abcd".substring(2)               //> x  : String = cd
	val y = "abcd".substring(2)               //> y  : String = cd
	x == y                                    //> res2: Boolean = true
}