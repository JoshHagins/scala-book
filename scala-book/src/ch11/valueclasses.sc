package ch11

object valueclasses {
	42.toString                               //> res0: String = 42
	
	42.hashCode                               //> res1: Int = 42
	
	42 equals 42                              //> res2: Boolean = true
}