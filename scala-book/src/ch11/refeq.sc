package ch11

object refeq {
  val x = new String("abc")                       //> x  : String = abc
  val y = new String("abc")                       //> y  : String = abc
  
  x == y                                          //> res0: Boolean = true
  x eq y                                          //> res1: Boolean = false
  x ne y                                          //> res2: Boolean = true
}