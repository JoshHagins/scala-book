package ch11

object implicitints {
  42 max 43                                       //> res0: Int = 43

  42 min 43                                       //> res1: Int = 42

  1 until 5                                       //> res2: scala.collection.immutable.Range = Range(1, 2, 3, 4)

  1 to 5                                          //> res3: scala.collection.immutable.Range.Inclusive = Range(1, 2, 3, 4, 5)

  3.abs                                           //> res4: Int = 3

  3.unary_-.abs                                   //> res5: Int = 3
}