package ch11

object bottomtypes {
  // val i: Int = null
  
  // Predef method error has return type Nothing
  def divide(x: Int, y: Int): Int =
  	if (y != 0) x / y
  	else error("can't divide by zero")        //> divide: (x: Int, y: Int)Int
}